/**
 *  Order model
 *  Describes the characteristics of each attribute in an order resource.
 *
 * @author Jyoshna Boppidi <S533706@nwmissouri.edu>
 *
 */

// bring in mongoose
// see <https://mongoosejs.com/> for more information
const mongoose = require("mongoose");
const Schema = mongoose.Schema

const OrderSchema = new mongoose.Schema({
  // _id: { type: Schema.Type.customer_id,
  // ref:_id,
  // required: true 
  // },
  _id: {
    type: Number,
    min: 1,
   max: 10000,
    required: true,
    unique: true,
    default: 555
  },
  datePlaced: {
    type: Date,
    required: true,
    default: Date.now()
  },
  dateShipped: {
    type: Date,
    required: false
  },
  paymentType: {
    type: String,
    // enum: ["not selected yet", "credit card", "cash", "check"],
    // <select>
    //         <option value="volvo">Volvo</option>,
    //         <option value="saab">Saab</option>
    //         <option value="mercedes">Mercedes</option>
    //         <option value="audi">Audi</option>
    //         </select>
            
    required: true,
  
  },
  amountDue: {
    type: String,
    required: true,
    default: 0
  },
  paid: {
    type: String
  },
  customer_id: {
    required: true,
    type: Number,
    min: 1,
    max: 10000,
    default: 3
  
  }
});
module.exports = mongoose.model("Order", OrderSchema);