/** 
*  Order Line Item model
*  Describes the characteristics of each attribute in an order line item - one entry on a customer's order.
*
* @author Chinmai sai Sure <S533977@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderLineItemSchema = new mongoose.Schema({

  _id: { type: Number,
   
    required: true },
  orderID: {
     type: Number,
     
      required: true },
    
    
  lineNumber: {
    type: Number,
    min:0,
    max:10000,
    required: true
  },
  productKey: {
    type: String,

    required: true
  },
  quantity: {
    type: Number,
    min:0,
    max:10000,
    required: true, 
    default: 1
  }
  ,
  expiry: {
    type: String,
    required: true,
    default: 'date'
   
  }

})
module.exports = mongoose.model('orderLineitem', OrderLineItemSchema)
