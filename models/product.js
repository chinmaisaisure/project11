/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Dominic Gillam <S516927@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  productKey: {
    type: String,
    required: true,
    unique: true,
    default: 'product'
  },
  description: {
    type: String,
    required: false,
    default: 'description'
  },
  unitPrice: {
    type: Number,
    required: true,
    default: 9.99,
    min: 0,
    max: 100000
  }
  ,
  expiry: {
    type: String,
    required: true,
    default: 'date'
   
  }
})
module.exports = mongoose.model('Product', ProductSchema)
