Title: Online Shopping

#Overview: 
In our application we are maintaining customer, order information and the product description. In this application we added our developers information under About tab.

Team project 06-11

Team members:

Chinmai Sai Sure - customer

Dominic Gilliam  - product

Jyoshna Boppidi - Order

Chinmai Sai sure - orderlineitem

Bitbucket repo link: https://bitbucket.org/chinmaisaisure/project11/src

In this app at first we added model and data.
Chinmai Sai Sure - added model and data to customer option.
Dominic Gilliam  - added model and data to product option.
Jyoshna Boppidi - added model and data to order option.
Chinmai Sai Sure - added model and data to orderlineitem option.

In the next step we added controllers,routes and views.
Chinmai Sai Sure - added header.ejs, footer.ejs,index.ejs,layout.ejs in the views folder.
Dominic Gilliam - added header.ejs, footer.ejs,index.ejs,layout.ejs in the views folder.
Jyoshna Boppidi - added header.ejs, footer.ejs,index.ejs,layout.ejs in the views folder.
Chinmai Sai Sure - added header.ejs, footer.ejs,index.ejs,layout.ejs in the views folder.

In the third step we added controller actions.
Chinmai Sai Sure - added controller actions like create,edit,delete icons.
Dominic Gilliam - added controller actions like create,edit,delete icons.
Jyoshna Boppidi - added controller actions like create,edit,delete icons.
Chinmai Sai Sure - added controller actions like create,edit,delete icons.


At last we added our information in about tab.
Chinmai Sai Sure - added our information in about option.
Dominic Gilliam - added our information in about option.
Jyoshna Boppidi - added our information in about option.
Chinmai Sai Sure - added our information in about option.